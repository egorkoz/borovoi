$(function() {
  $(document).ready(function () {
    //initialize swiper when document ready
    var mySwiper = new Swiper ('.swiper-container', {
      // Optional parameters
      loop: true,
      pagination: {
        el: '.swiper-pagination',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      autoplay: {
    	delay: 7500,
  	  }
    })
  });
});

$(document).ready(function(){
    $(".find").click(function(){
        $(".search").toggleClass("active");
    });
});